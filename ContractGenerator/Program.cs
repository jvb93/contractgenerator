﻿using Castle.Sharp2Js;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ContractGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new JsGeneratorOptions()
            {
                CamelCase = true,
                OutputNamespace = "models",
                IncludeMergeFunction = true,
                RespectDataMemberAttribute = true,
                RespectDefaultValueAttribute = true,
                TreatEnumsAsStrings = false,
            };

            var assemblyName = "Contracts";
            var nameSpace = "Contracts";

            var asm = Assembly.Load(assemblyName);
            var classes = asm.GetTypes().Where(p =>
                p.Namespace == nameSpace
            ).ToArray();

            var str = JsGenerator.Generate(classes, options);
            File.WriteAllText("models.js", str);
        }
    }
}