﻿using System;

namespace Contracts
{
    public abstract class Vehicle
    {
        private int _numberOfSeats;
        public int NumberOfSeats
        {
            get => _numberOfSeats;
            set => _numberOfSeats = Math.Abs(value);
        }
    }
}
