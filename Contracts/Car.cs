﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Contracts
{
    public class Car : Vehicle
    {
        public string CarpetColor { get; set; }
        public CarType Type { get; set; }

        public Car()
        {
            Type = CarType.Sedan;
            CarpetColor = "Black";
        }
    }

    public enum CarType
    {
        Coupe,
        Sedan
    }
}
